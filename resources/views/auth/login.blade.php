<x-welcome.master>
  <main id="main">
  
    <!-- ======= Contact Us Section ======= -->
    <section id="contact" class="contact">
      <div class="container">
  
        <div class="section-title">
          <h2>Sign in</h2>
        </div>
  
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <form action="{{ route('login') }}" method="post" class="php-email-form" style="padding-top: 0px;">
              @csrf
              <div class="row">
                <div class="col-md-6 mt-3">
                  <a href="/" title="Back to home"><h3><i class="bi bi-arrow-left-square-fill p5"></i></h3></a>
                </div>
              </div>

              @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <p class="text-danger"><i class="bi bi-x-lg"></i> {{ $error }}</p>
                @endforeach
              @endif              

              <div class="form-group">
                <input type="text" name="identity" class="form-control" id="identity" placeholder="Email / Username" value="{{ old('identity') }}" required autocomplete="off">
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="{{ old('password') }}" required autocomplete="off">
              </div>
              <div class="text-center"><button type="submit"><i class="bi bi-send-check-fill"></i> Sign in</button></div>
            </form>
          </div>
  
        </div>
  
      </div>
    </section><!-- End Contact Us Section -->
  
  </main><!-- End #main -->
  <br><br><br><br><br><br><br><br><br><br>

  @push('style')
  <style>
    option {
      color: black;
    }
  </style>
  @endpush

</x-welcome.master>