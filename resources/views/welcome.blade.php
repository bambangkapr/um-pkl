<x-welcome.master>
  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex flex-column align-items-center">

      <h1>ComingSoon</h1>
      <h2>Universitas Mandiri - Subang</h2>
      <div class="countdown d-flex justify-content-center" data-count="2024/7/31">
        <div>
          <h3>%d</h3>
          <h4>Days</h4>
        </div>
        <div>
          <h3>%h</h3>
          <h4>Hours</h4>
        </div>
        <div>
          <h3>%m</h3>
          <h4>Minutes</h4>
        </div>
        <div>
          <h3>%s</h3>
          <h4>Seconds</h4>
        </div>
      </div>

      <div class="social-links text-center">
        <a href="{{ route('login') }}" class="twitter btn btn-outline-secondary text-danger" title="Login"><h6><i class="bi bi-box-arrow-in-right"></i> Sign in</h6></a>
        <a href="{{ route('registration') }}" class="twitter btn btn-outline-danger" title="Register"><h6><i class="bi bi-check-square-fill"></i> Sign up</h6></a>
      </div>

    </div>
  </header><!-- End #header -->
</x-welcome.master>