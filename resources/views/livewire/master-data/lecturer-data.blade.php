@section('title', 'Lecturer')
@section('master-data-menu', 'menu-open')
@section('master-data', 'active')
@section('lecturer', 'active') 
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Lecturer</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="#">Admin</a>
            </li>
            <li class="breadcrumb-item">
              <a href="#">Master Data</a>
            </li>
            <li class="breadcrumb-item active">Lecturer</li>
          </ol>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- Default box -->
          <div class="card card-warning card-outline bg-secondary">
            <div class="card-header">
              <button wire:click="addNew" type="button" class="btn btn-warning"><i class="fas fa-plus"></i> Add Data</button>
    
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="d-flex justify-content-between">
                    <div>
                      <div class="form-group">
                          <select wire:model.live="paginate" class="form-control">
                              <option value="10">10</option>
                              <option value="15">15</option>
                              <option value="20">20</option>
                              <option value="30">30</option>
                              <option value="50">50</option>
                          </select>
                      </div>
                    </div>

                  <div>
                    <div class="input-group mb-3">
                      <input wire:model.live="search" type="text" class="form-control" placeholder="Search...">
                      <div class="input-group-append">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                      </div>
                    </div>
                  </div>
              </div>
  
              <div class="table-responsive-sm">
                <table class="table table-sm table-striped mt-1">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Name</th>
                            <th>NIDN</th>
                            <th>College</th>
                            <th>Prodi Homebase</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      @forelse ($datas as $key => $data)
                        <tr class="text-center">
                            <td>{{ $datas->firstItem() + $key }}</td>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->nidn }}</td>
                            <td>{{ $data->college }}</td>
                            <td>{{ $data->prodi_homebase }}</td>
                            <td>{{ $data->contact }}</td>
                            <td>{{ $data->user->email }}</td>
                            <td>
                              <button wire:click.prevent="edit('{{ $data->id }}')" class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                              <button wire:click.prevent="delete('{{ $data->id }}')" class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                      @empty
                        <tr>
                            <td colspan="9" class="text-center font-italic text-danger"><h5>-- Data Tidak Ditemukan --</h5></td>
                        </tr>
                      @endforelse
                    </tbody>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              {{ $datas->links() }}
            </div>
            <!-- /.card-footer-->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>
  </section>

  <input wire:model.live="form" type="text" id="formDesc" style="display: none;">

  <!-- Modal Add / Edit -->
  <div class="modal fade" id="form" wire:ignore.self>
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header {{ $form == 'add' ? 'bg-warning':'bg-info' }}">
          <h4 class="modal-title text-light">{{ $form == 'add' ? 'Add':'Edit' }} Lecturer</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @if($form == 'add')
        <form wire:submit="createData">
        @else
        <form wire:submit="updateData">
        @endif
          <div class="modal-body bg-secondary">

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input wire:model="name" required class="form-control @error('name') is-invalid @enderror" type="text" placeholder="Name" id="name" autocomplete="off">
                  @error('name')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="nidn">NIDN</label>
                  <input wire:model="nidn" required class="form-control @error('nidn') is-invalid @enderror" type="text" placeholder="NIDN" id="nidn" autocomplete="off">
                  @error('nidn')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="college">College</label>
                  <input wire:model="college" required class="form-control @error('college') is-invalid @enderror" type="text" placeholder="College" id="college" autocomplete="off">
                  @error('college')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="prodi_homebase">Prodi Homebase</label>
                  <input wire:model="prodi_homebase" required class="form-control @error('prodi_homebase') is-invalid @enderror" type="text" placeholder="Prodi Homebase" autocomplete="off">
                  @error('prodi_homebase')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="contact">Contact</label>
                  <input wire:model="contact" required class="form-control @error('contact') is-invalid @enderror" type="text" placeholder="Contact" id="contact" autocomplete="off">
                  @error('contact')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input wire:model="email" required class="form-control @error('email') is-invalid @enderror" type="email" placeholder="Email" id="email" autocomplete="off">
                  @error('email')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>
              
              @if($form == 'add')
              <div class="col-md-6">
                <div class="form-group">
                  <label for="password">Password</label>
                  <input wire:model="password" required class="form-control @error('password') is-invalid @enderror" type="password" placeholder="password" id="password" autocomplete="off">
                  @error('password')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="password">Confirm Password</label>
                  <input type="password" wire:model="password_confirmation" class="form-control @error('password') is-invalid @enderror" id="password_confirmation" placeholder="Confirm Password Password" required>
                  @error('password')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                  @enderror
                </div>
              </div>
              @endif
            </div> 
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            @if($form == 'add')
            <button type="submit" class="btn btn-warning"><i class="fas fa-paper-plane"></i> Save</button>
            @else
            <button type="submit" class="btn btn-info"><i class="fas fa-paper-plane"></i> Update</button>
            @endif
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <!-- Modal DELETE -->
  <div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-danger">
          <h4 class="modal-title">Confirm delete data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <h5>Are you sure you delete data?</h5>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button wire:click.prevent="deleteData" type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</div>


@push('styles')
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush

@push('scripts')
<!-- SweetAlert2 -->
<script src="{{ asset('assets/admin/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Sweet alert real rashid -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>

  $(function () {

    window.addEventListener('show-form', event => {
        $('#form').modal('show');
    });

    window.addEventListener('hide-form', event => {

      const form = $('#formDesc').val();
      if(form == 'add') {
        $('#form').modal('hide');
        showAlert('Lecturer data has been successfully added!')
      } else if(form == 'edit') {
        $('#form').modal('hide');
        showAlert('Lecturer data has been successfully updated!')
      } else {
        $('#modal-delete').modal('hide');
        showAlert('Lecturer data has been successfully deleted!')
      }

    });

    window.addEventListener('show-modal-delete', event => {
        $('#modal-delete').modal('show');
    });

    async function showAlert(message) {
      Swal.fire({
          "title":'<span class="text-success">Success!</span>',
          "text":message,
          "position":"middle-center",
          "timer":2000,
          "width":"32rem",
          "heightAuto":true,
          "padding":"1.25rem",
          "showConfirmButton":false,
          "showCloseButton":false,
          "icon":'success',
          "background": "#fff"
      });
    }

  });
</script>
@endpush