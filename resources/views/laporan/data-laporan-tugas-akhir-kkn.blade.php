<x-admin.master>

  @section('title', 'Data Laporan Tugas Akhir KKN')
  @section('menu-laporan', 'menu-open')
  @section('laporan', 'active')
  @section('data-laporan-tugas-akhir-kkn', 'active')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Laporan Tugas Akhir KKN</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Admin</a></li>
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active">Data Laporan Tugas Akhir KKN</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card card-warning card-outline bg-secondary">
              <div class="card-header">
                <button type="button" class="btn btn-success"><i class="fas fa-file-excel"></i> Download</button>
      
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex justify-content-between">
                      <div>
                        <div class="form-group">
                            <select wire:model.live="paginate" class="form-control">
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                      </div>

                    <div>
                      <div class="input-group mb-3">
                        <input wire:model.live="search" type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                      </div>
                    </div>
                </div>
    
                <div class="table-responsive-sm">
                  <table class="table table-sm table-striped mt-1">
                      <thead>
                          <tr class="text-center">
                              <th>#</th>
                              <th>Tahun</th>
                              <th>Bulan</th>
                              <th>Tautan</th>
                              <th>Aksi</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr class="text-center">
                              <td>1</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                          <tr class="text-center">
                              <td>2</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                          <tr class="text-center">
                              <td>3</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                          <tr class="text-center">
                              <td>4</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                          <tr class="text-center">
                              <td>5</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- /.card-footer-->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
</x-admin.master>