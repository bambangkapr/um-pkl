<x-admin.master>

  @section('title', 'Data Laporan')
  @section('menu-laporan', 'menu-open')
  @section('laporan', 'active')
  @section('data-laporan', 'active')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Laporan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Admin</a></li>
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active">Data Laporan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card card-warning card-outline bg-secondary">
              <div class="card-body">
                <div class="d-flex justify-content-between">
                      <div>
                        <div class="form-group">
                            <select wire:model.live="paginate" class="form-control">
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                      </div>

                    <div>
                      <div class="input-group mb-3">
                        <input wire:model.live="search" type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                      </div>
                    </div>
                </div>
    
                <div class="table-responsive-sm">
                  <table class="table table-sm table-striped mt-1">
                      <thead>
                          <tr class="text-center">
                              <th>#</th>
                              <th>Tahun</th>
                              <th>Bulan</th>
                              <th>Tautan</th>
                              <th>Aksi</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr class="text-center">
                              <td>1</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                          <tr class="text-center">
                              <td>2</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                          <tr class="text-center">
                              <td>3</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                          <tr class="text-center">
                              <td>4</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                          <tr class="text-center">
                              <td>5</td>
                              <td>2024</td>
                              <td>Juli</td>
                              <td>Link Google Drive</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- /.card-footer-->
            </div>

            <div class="card card-warning card-outline bg-secondary">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Pilih Tahun</label>
                      <input type="text" class="form-control" placeholder="Pilih Tahun">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Pilih Bulan</label>
                      <input type="text" class="form-control" placeholder="Pilih Bulan">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>&nbsp;</label>
                      <button class="btn btn-warning d-block">Filter Data</button>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-check"></i> Panduan Pengisian</h5>
                  <ol>
                    <li>Bagaimana aktifitas mentoring dan koordinasi dengan mahasiswa maupun perangkat desa dan atau kecamatan ?</li>
                    <li>Tantangan apa yang dihadapi selama di lokasi dan berikan alternatif solusi untuk menghadapainya ?</li>
                    <li>Apa saja dan jelaskan pengembangan kompetensi (hardskill maupun softskill) mahasiswa yang telah dicapai ?</li>
                    <li>Bagaimana capaian KPI yang telah ditetapkan berdasarkan program kerja para mahasiswa dan berikan penjelasannya?</li>
                    <li>minimal 200 kata (angka dan tanda baca tidak di hitung kata).</li>
                  </ol>
                </div>
                <p class="log">Log Bulan: Juli 2024</p>
                <textarea id="summernote"></textarea>
                <hr>
                <div class="form-group">
                  <div class="alert alert-warning">
                    <p>Keterangan : Tautan google drive yang berisikan dokumen laporan bulanan</p>
                  </div>
                  <label>Tautan Dokumen</label>
                  <input type="text" class="form-control" placeholder="Link/Tautan Dokumen">
                </div>
                <button type="submit" class="btn btn-warning">Simpan</button>
                <div class="alert alert-info mt-5">
                  <p>Keterangan : Tautan google drive yang berisikan dokumen laporan bulanan</p>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  @push('styles')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.css') }}">
  @endpush
  @push('scripts')
    <!-- Summernote -->
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
      $(function () {
        // Summernote
        $('#summernote').summernote({
          placeholder: 'Silakan Isi',
          toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link']],
            ['view', ['fullscreen']],
          ]
        })
      })
    </script>
  @endpush
</x-admin.master>