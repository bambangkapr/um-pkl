<x-admin.master>

  @section('title', 'Kelola Data Mentoring Mahasiswa')
  @section('menu-laporan', 'menu-open')
  @section('laporan', 'active')
  @section('kelola-data-mentoring-mahasiswa', 'active')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kelola Data Mentoring Mahasiswa</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Admin</a></li>
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active">Kelola Data Mentoring Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card card-warning card-outline bg-secondary">
              <div class="card-header">
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-default"><i class="fas fa-plus"></i> Add Data</button>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex justify-content-between">
                      <div>
                        <div class="form-group">
                            <select wire:model.live="paginate" class="form-control">
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                      </div>

                    <div>
                      <div class="input-group mb-3">
                        <input wire:model.live="search" type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                      </div>
                    </div>
                </div>
    
                <div class="table-responsive-sm">
                  <table class="table table-sm table-striped mt-1">
                      <thead>
                          <tr class="text-center">
                              <th>#</th>
                              <th>NIM</th>
                              <th>Nama</th>
                              <th>Perguruan Tinggi</th>
                              <th>Prodi</th>
                              <th>Aksi</th>
                              <th>Nilai Log Bulanan</th>
                              <th>Nilai & Free Form</th>
                              <th>Tugas Akhir</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr class="text-center">
                              <td>1</td>
                              <td>12345</td>
                              <td>Nama Satu</td>
                              <td>Universitas Satu</td>
                              <td>Jurusan Satu</td>
                              <td>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>
                                <a href="#" data-toggle="modal" data-target="#modal-nilai">rekap nilai</a>
                              </td>
                              <td>
                                <a href="{{ route('data-nilai-konversi-dan-free-form', 123) }}">lihat data</a>
                              </td>
                              <td>link google drive</td>
                          </tr>
                          <tr class="text-center">
                              <td>2</td>
                              <td>12345</td>
                              <td>Nama Dua</td>
                              <td>Universitas Dua</td>
                              <td>Jurusan Dua</td>
                              <td>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>rekap nilai</td>
                              <td>lihat data</td>
                              <td>link google drive</td>
                          </tr>
                          <tr class="text-center">
                              <td>3</td>
                              <td>12345</td>
                              <td>Nama Tiga</td>
                              <td>Universitas Tiga</td>
                              <td>Jurusan Tiga</td>
                              <td>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>rekap nilai</td>
                              <td>lihat data</td>
                              <td>link google drive</td>
                          </tr>
                          <tr class="text-center">
                              <td>4</td>
                              <td>12345</td>
                              <td>Nama Empat</td>
                              <td>Universitas Empat</td>
                              <td>Jurusan Empat</td>
                              <td>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>rekap nilai</td>
                              <td>lihat data</td>
                              <td>link google drive</td>
                          </tr>
                          <tr class="text-center">
                              <td>5</td>
                              <td>12345</td>
                              <td>Nama Lima</td>
                              <td>Universitas Lima</td>
                              <td>Jurusan Lima</td>
                              <td>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>rekap nilai</td>
                              <td>lihat data</td>
                              <td>link google drive</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- /.card-footer-->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <h4 class="modal-title">Add Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="#" method="get">
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" placeholder="Name">
            </div>
          </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-warning"><i class="fas fa-paper-plane"></i> Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <div class="modal fade" id="modal-nilai">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <h4 class="modal-title">Rekap Nilai Log Bulanan</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>januari</th>
                <th>februari</th>
                <th>maret</th>
                <th>april</th>
                <th>mei</th>
                <th>juni</th>
                <th>juli</th>
                <th>agustus</th>
                <th>september</th>
                <th>oktober</th>
                <th>november</th>
                <th>desember</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td>9</td>
                <td></td>
                <td>9</td>
                <td></td>
                <td>9</td>
                <td></td>
                <td></td>
                <td>9</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>8</td>
                <td></td>
                <td></td>
                <td>90</td>
                <td></td>
                <td>9</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>6</td>
                <td></td>
              </tr>
              <tr>
                <td>9</td>
                <td>10</td>
                <td></td>
                <td></td>
                <td>8</td>
                <td>9</td>
                <td></td>
                <td></td>
                <td>9</td>
                <td></td>
                <td>8</td>
                <td>5</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-warning"><i class="fas fa-paper-plane"></i> Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</x-admin.master>