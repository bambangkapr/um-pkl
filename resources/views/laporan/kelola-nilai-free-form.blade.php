<x-admin.master>

  @section('title', 'Kelola Nilai Free Form')
  @section('menu-laporan', 'menu-open')
  @section('laporan', 'active')
  @section('kelola-nilai-free-form', 'active')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kelola Nilai Free Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Admin</a></li>
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active">Kelola Nilai Free Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card card-warning card-outline bg-secondary">
              <div class="card-header">
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-default"><i class="fas fa-plus"></i> Add Data</button>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex justify-content-between">
                      <div>
                        <div class="form-group">
                            <select wire:model.live="paginate" class="form-control">
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                      </div>

                    <div>
                      <div class="input-group mb-3">
                        <input wire:model.live="search" type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                      </div>
                    </div>
                </div>
    
                <div class="table-responsive-sm">
                  <table class="table table-sm table-striped mt-1">
                      <thead>
                          <tr class="text-center">
                              <th>#</th>
                              <th>Aksi</th>
                              <th>NIM</th>
                              <th>Nama</th>
                              <th>Perguruan Tinggi</th>
                              <th>Prodi</th>
                              <th>Free Form</th>
                              <th>Nilai DPL</th>
                              <th>Nilai DPA</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr class="text-center">
                              <td>1</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>12345</td>
                              <td>Nama Satu</td>
                              <td>Universitas Satu</td>
                              <td>Jurusan Satu</td>
                              <td>Pengembangan kepribadian mahasiswa (Personality Development)</td>
                              <td>90</td>
                              <td>80</td>
                          </tr>
                          <tr class="text-center">
                              <td>2</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>12345</td>
                              <td>Nama Dua</td>
                              <td>Universitas Dua</td>
                              <td>Jurusan Dua</td>
                              <td>Pengembangan kepribadian mahasiswa (Personality Development)</td>
                              <td>90</td>
                              <td>80</td>
                          </tr>
                          <tr class="text-center">
                              <td>3</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>12345</td>
                              <td>Nama Tiga</td>
                              <td>Universitas Tiga</td>
                              <td>Jurusan Tiga</td>
                              <td>Pengembangan kepribadian mahasiswa (Personality Development)</td>
                              <td>90</td>
                              <td>80</td>
                          </tr>
                          <tr class="text-center">
                              <td>4</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>12345</td>
                              <td>Nama Empat</td>
                              <td>Universitas Empat</td>
                              <td>Jurusan Empat</td>
                              <td>Pengembangan kepribadian mahasiswa (Personality Development)</td>
                              <td>90</td>
                              <td>80</td>
                          </tr>
                          <tr class="text-center">
                              <td>5</td>
                              <td>
                                <button class="btn btn-info btn-sm" title="Edit"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" title="Hapus"><i class="fas fa-trash"></i></button>
                              </td>
                              <td>12345</td>
                              <td>Nama Lima</td>
                              <td>Universitas Lima</td>
                              <td>Jurusan Lima</td>
                              <td>Pengembangan kepribadian mahasiswa (Personality Development)</td>
                              <td>90</td>
                              <td>80</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- /.card-footer-->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <h4 class="modal-title">Add Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Pilih Mahasiswa</label>
                <select class="form-control">
                  <option value="">Mahasiswa Satu</option>
                  <option value="">Mahasiswa Dua</option>
                  <option value="">Mahasiswa Tiga</option>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Free Form</label>
                <select class="form-control">
                  <option value="">Pengembangan kepribadian mahasiswa (Personality Development)</option>
                  <option value="">Kreativitas</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Nilai DPL : (A >= 80 ; B 70 -79 ; C < 70)</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Nilai DPA : (A >= 80 ; B 70 -79 ; C < 70)</label>
                <input type="text" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-warning"><i class="fas fa-paper-plane"></i> Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</x-admin.master>