<x-admin.master>

  @section('title', 'Data Nilai Konversi dan Free Form')
  @section('menu-laporan', 'menu-open')
  @section('laporan', 'active')
  @section('kelola-data-mentoring-mahasiswa', 'active')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Nilai Konversi dan Free Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Admin</a></li>
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item"><a href="{{ route('kelola-data-mentoring-mahasiswa') }}">Kelola Data</a></li>
              <li class="breadcrumb-item active">Data Nilai Konversi dan Free Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card card-warning card-outline bg-secondary">
              <div class="card-header">
                DEWI MELLENIA | 2111700003 | SISTEM INFORMASI | Universitas Mandiri

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <h3>Data Nilai Structure form</h3>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Matakuliah</th>
                      <th>SKS</th>
                      <th>Nilai DPL</th>
                      <th>Nilai DPA</th>
                      <th>Nilai Akhir</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Matakuliah 1</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Matakuliah 2</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Matakuliah 3</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Matakuliah 4</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Matakuliah 5</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                  </tbody>
                </table>

                <br>

                <h3>Data Nilai Structure form</h3>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Matakuliah</th>
                      <th>SKS</th>
                      <th>Nilai DPL</th>
                      <th>Nilai DPA</th>
                      <th>Nilai Akhir</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Matakuliah 1</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Matakuliah 2</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Matakuliah 3</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Matakuliah 4</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Matakuliah 5</td>
                      <td>8</td>
                      <td>6</td>
                      <td>8</td>
                      <td>9</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
</x-admin.master>