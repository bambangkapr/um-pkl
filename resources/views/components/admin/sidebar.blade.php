<aside class="main-sidebar sidebar-dark-warning elevation-4">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <img src="{{ asset('assets/welcome/img/um.png') }}" alt="UM" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">PKL | UM</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('assets/admin/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="{{ route('profile-data') }}" class="d-block">{{ Auth::user()->name }}</a>
        @if(Auth::user()->roles)
          @foreach(Auth::user()->roles as $role)
            <span class="badge badge-success">{{ $role->display_name }}</span>
          @endforeach
        @endif
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-flat" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
              with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/dashboard" class="nav-link @yield('dashboard')">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('data-perguruan-tinggi') }}" class="nav-link @yield('data-perguruan-tinggi')">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Data Perguruan Tinggi
            </p>
          </a>
        </li>
        <li class="nav-item @yield('menu-kelola-laporan')">
          <a href="#" class="nav-link @yield('kelola-laporan')">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Kelola Laporan
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('log-harian-mahasiswa') }}" class="nav-link @yield('log-harian-mahasiswa')">
                <i class="far fa-circle nav-icon"></i>
                <p>Log Harian Mahasiswa</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('log-bulanan-mahasiswa') }}" class="nav-link @yield('log-bulanan-mahasiswa')">
                <i class="far fa-circle nav-icon"></i>
                <p>Log Bulanan Mahasiswa</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('log-kehadiran-mahasiswa') }}" class="nav-link @yield('log-kehadiran-mahasiswa')">
                <i class="far fa-circle nav-icon"></i>
                <p>Log Kehadiran Mahasiswa</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('laporan-capaian-kpi') }}" class="nav-link @yield('laporan-capaian-kpi')">
                <i class="far fa-circle nav-icon"></i>
                <p>Laporan Capaian KPI</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item @yield('menu-laporan')">
          <a href="#" class="nav-link @yield('laporan')">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Laporan
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('data-laporan') }}" class="nav-link @yield('data-laporan')">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Laporan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('data-laporan-tugas-akhir-kkn') }}" class="nav-link @yield('data-laporan-tugas-akhir-kkn')">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Laporan Tugas Akhir KKN</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('kelola-data-mentoring-mahasiswa') }}" class="nav-link @yield('kelola-data-mentoring-mahasiswa')">
                <i class="far fa-circle nav-icon"></i>
                <p>Kelola Data Mentoring Mahasiswa</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('kelola-nilai-structure-form') }}" class="nav-link @yield('kelola-nilai-structure-form')">
                <i class="far fa-circle nav-icon"></i>
                <p>Kelola Nilai Structure Form</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('kelola-nilai-free-form') }}" class="nav-link @yield('kelola-nilai-free-form')">
                <i class="far fa-circle nav-icon"></i>
                <p>Kelola Nilai Free Form</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item @yield('master-data-menu')">
          <a href="#" class="nav-link @yield('master-data')">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Master Data
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('study-program-data') }}" class="nav-link @yield('study-program')">
                <i class="far fa-circle nav-icon"></i>
                <p>Study Program</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('program-data') }}" class="nav-link @yield('program')">
                <i class="far fa-circle nav-icon"></i>
                <p>Program</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('lecturer-data') }}" class="nav-link @yield('lecturer')">
                <i class="far fa-circle nav-icon"></i>
                <p>Lecturer</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-header">&nbsp;</li>
        <li class="nav-item">
          <form action="{{ route('logout') }}" method="post" id="formLogout">
            @csrf
            <a href="#" class="nav-link" id="logout">
              <i class="nav-icon fas fa-power-off text-danger"></i>
              <p>Sign out</p>
            </a>
          </form>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>