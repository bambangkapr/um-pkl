<x-welcome.master>
  <main id="main">
  
    <!-- ======= Contact Us Section ======= -->
    <section id="contact" class="contact">
      <div class="container">
  
        <div class="section-title">
          <h2>Pendaftaran</h2>
        </div>
  
        <div class="row justify-content-center">
          <div class="col-lg-10">
            <form action="{{ route('saveRegistration') }}" method="post" class="php-email-form">
              @csrf
              <div class="row">
                <div class="col-md-6 mt-3">
                  <a href="/" title="Back to home"><h3><i class="bi bi-arrow-left-square-fill p5"></i></h3></a>
                </div>
              </div>
              <div class="row mt-1">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nama Lengkap" value="{{old('name')}}" required autocomplete="off">
                  @error('name')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{old('email')}}" required autocomplete="off">
                  @error('email')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
                <div class="col-md-6 form-group">
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                  @error('password')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
                <div class="col-md-6 form-group">
                  <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Konfirmasi Password" required>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="text" class="form-control" name="nim" id="nim" placeholder="NIM" required value="{{ old('nim') }}" autocomplete="off">
                  @error('nim')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <select name="study_program_id" id="study_program_id" class="form-control program-studi" required>
                      <option value="">Pilih Program Studi</option>
                      @foreach($studies as $studi)
                      <option value="{{ $studi->id }}" {{ old('study_program_id') == $studi->id ? 'selected':'' }}>{{ $studi->name }}</option>
                      @endforeach
                  </select>
                  @error('study_program_id')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <select name="semester" class="form-control program-studi" required>
                      <option value="">Pilih Semester</option>
                      <option value="1" {{ old('semester') == 1 ? 'selected':'' }}>Semester ke-1</option>
                      <option value="2" {{ old('semester') == 2 ? 'selected':'' }}>Semester ke-2</option>
                      <option value="3" {{ old('semester') == 3 ? 'selected':'' }}>Semester ke-3</option>
                      <option value="4" {{ old('semester') == 4 ? 'selected':'' }}>Semester ke-4</option>
                      <option value="5" {{ old('semester') == 5 ? 'selected':'' }}>Semester ke-5</option>
                      <option value="6" {{ old('semester') == 6 ? 'selected':'' }}>Semester ke-6</option>
                      <option value="7" {{ old('semester') == 7 ? 'selected':'' }}>Semester ke-7</option>
                      <option value="8" {{ old('semester') == 8 ? 'selected':'' }}>Semester ke-8</option>
                      <option value="9" {{ old('semester') == 9 ? 'selected':'' }}>Semester ke-9</option>
                      <option value="10" {{ old('semester') == 10 ? 'selected':'' }}>Semester ke-10</option>
                      <option value="11" {{ old('semester') == 11 ? 'selected':'' }}>Semester ke-11</option>
                      <option value="12" {{ old('semester') == 12 ? 'selected':'' }}>Semester ke-12</option>
                      <option value="13" {{ old('semester') == 13 ? 'selected':'' }}>Semester ke-13</option>
                      <option value="14" {{ old('semester') == 14 ? 'selected':'' }}>Semester ke-14</option>
                  </select>
                  @error('semester')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <select name="program_id" class="form-control program-studi" required>
                      <option value="">Pilih Program</option>
                      @foreach($programs as $program)
                      <option value="{{ $program->id }}" {{ old('program_id') == $program->id ? 'selected':'' }}>{{ $program->name }}</option>
                      @endforeach
                  </select>
                  @error('program_id')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="reason" rows="5" placeholder="Alasan mengikuti program tersebut" required>{{ old('reason') }}</textarea>
                @error('reason')
                <p class="text-danger">{{ $message }}</p>
                @enderror
              </div>
              <div class="text-center"><button type="submit"><i class="bi bi-send-check-fill"></i> Daftar</button></div>
            </form>
          </div>
  
        </div>
  
      </div>
    </section><!-- End Contact Us Section -->
  
  </main><!-- End #main -->

  @push('style')
  <style>
    option {
      color: red;
    }
  </style>
  @endpush

</x-welcome.master>