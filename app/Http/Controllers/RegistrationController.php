<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Program;
use App\Models\Student;
use App\Models\StudyProgram;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;
use RealRashid\SweetAlert\Facades\Alert;

class RegistrationController extends Controller
{
    public function registration()
    {
        // $user = User::find(1);
        // $user->addRole('admin');
        return view('registration', [
            'studies' => StudyProgram::all(),
            'programs' => Program::all()
        ]);
    }

    public function saveRegistration(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|min:8|confirmed',
            'nim' => 'required|max:30',
            'study_program_id' => 'required|max:255',
            'semester' => 'required|max:255',
            'program_id' => 'required|max:255',
            'reason' => 'required|max:1000',
        ]);

        $dataUser = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ];

        $dataStudent = [
            'name' => $request->name,
            'nim' => $request->nim,
            'study_program_id' => $request->study_program_id,
        ];

        $dataActivity = [
            'semester' => $request->semester,
            'program_id' => $request->program_id,
            'reason' => $request->reason,
        ];

        DB::beginTransaction();
        try {
            $user = User::create($dataUser);
            $user->addRole('peserta');

            $dataStudent['user_id'] = $user->id;
            $student = Student::create($dataStudent);

            $dataActivity['student_id'] = $student->id;
            Activity::create($dataActivity);

            DB::commit();

            Alert::success('Berhasil', 'Pendaftaran PKL berhasil!');
            return redirect('/');
        } catch (Exception $e) {
            DB::rollBack();
            Alert::error('Error', 'Error: ' . $e->getMessage());
        }
    }
}
