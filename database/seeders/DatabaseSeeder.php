<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        // User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        DB::table('users')->insert([
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
        ]);

        $this->call(LaratrustSeeder::class);

        DB::table('study_programs')->insert([
            [ 'name' => 'Fisika' ],
            [ 'name' => 'Teknik Informatika' ],
            [ 'name' => 'Sistem Informasi' ],
            [ 'name' => 'Teknik Komputer Jaringan' ]
        ]);

        DB::table('programs')->insert([
            [ 'name' => 'PKL Reguler (2 SKS)' ],
            [ 'name' => 'Magang/Riset (MBKM) (20 SKS)' ]
        ]);
    }
}
