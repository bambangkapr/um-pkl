<?php

use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RegistrationController;
use App\Livewire\MasterData\LecturerData;
use App\Livewire\MasterData\ProgramData;
use App\Livewire\MasterData\StudyProgramData;
use App\Livewire\ProfileData;

Route::redirect('/register', '/');
Route::redirect('/up', '/');
Route::redirect('/user/confirm-password', '/');
Route::redirect('/user/confirmed-password-status', '/');
Route::redirect('/user/profile', '/');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/registration', [RegistrationController::class, 'registration'])->name('registration');
Route::post('/saveRegistration', [RegistrationController::class, 'saveRegistration'])->name('saveRegistration');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/profile-data', ProfileData::class)->name('profile-data');

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::view('/data-perguruan-tinggi', 'data-perguruan-tinggi')->name('data-perguruan-tinggi');

    Route::prefix('kelola-laporan')->group(function () {
        Route::view('/log-harian-mahasiswa', 'kelola-laporan/log-harian-mahasiswa')->name('log-harian-mahasiswa');
        Route::view('/log-bulanan-mahasiswa', 'kelola-laporan/log-bulanan-mahasiswa')->name('log-bulanan-mahasiswa');
        Route::view('/log-kehadiran-mahasiswa', 'kelola-laporan/log-kehadiran-mahasiswa')->name('log-kehadiran-mahasiswa');
        Route::view('/laporan-capaian-kpi', 'kelola-laporan/laporan-capaian-kpi')->name('laporan-capaian-kpi');
    });

    Route::prefix('laporan')->group(function () {
        Route::view('/data-laporan', 'laporan/data-laporan')->name('data-laporan');
        Route::view('/data-laporan-tugas-akhir-kkn', 'laporan/data-laporan-tugas-akhir-kkn')->name('data-laporan-tugas-akhir-kkn');
        Route::view('/kelola-data-mentoring-mahasiswa', 'laporan/kelola-data-mentoring-mahasiswa')->name('kelola-data-mentoring-mahasiswa');
        Route::view('/kelola-nilai-structure-form', 'laporan/kelola-nilai-structure-form')->name('kelola-nilai-structure-form');
        Route::view('/kelola-nilai-free-form', 'laporan/kelola-nilai-free-form')->name('kelola-nilai-free-form');

        Route::view('/data-nilai-konversi-dan-free-form/{id}', 'laporan/data-nilai-konversi-dan-free-form')->name('data-nilai-konversi-dan-free-form');
    });

    Route::prefix('master-data')->group(function () {
        Route::get('/study-program-data', StudyProgramData::class)->name('study-program-data');
        Route::get('/program-data', ProgramData::class)->name('program-data');
        Route::get('/lecturer-data', LecturerData::class)->name('lecturer-data');
    });
});
